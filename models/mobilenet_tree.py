import torch
import torch.nn as nn
import torch.nn.init as init
import torch.nn.functional as F
from torch.autograd import Variable
import sys
import numpy as np
import random
import types
import math
from flops_counter import *

"""
 MobileNet Tree Implementation
"""

def _make_divisible(v, divisor, min_value=None):
    """
    This function ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    :param v:
    :param divisor:
    :param min_value:
    :return:
    """
    if min_value is None:
        min_value = divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v

### Conv+BN+ReLU block definition
class ConvBNReLU(nn.Sequential):
    def __init__(self, in_planes, out_planes, kernel_size=3, stride=1, groups=1, use_relu = True):
        padding = (kernel_size - 1) // 2
        if(use_relu):
            super(ConvBNReLU, self).__init__(
                nn.Conv2d(in_planes, out_planes, kernel_size, stride, padding, groups=groups, bias=False),
                nn.BatchNorm2d(out_planes),
                nn.ReLU6(inplace=True)
            )
        else:
            super(ConvBNReLU, self).__init__(
                nn.Conv2d(in_planes, out_planes, kernel_size, stride, padding, groups=groups, bias=False),
                nn.BatchNorm2d(out_planes),
            )

### Inverted Residual Block Definition
class InvertedResidual(nn.Module):
    def __init__(self, inp, oup, stride, expand_ratio, expansion, groups):
        super(InvertedResidual, self).__init__()
        self.stride = stride
        assert stride in [1, 2]
        self.expansion = expansion
        self.groups = groups
        self.use_res_connect = self.stride == 1 and inp == oup
        inp = inp*groups
        oup = oup*groups*(2**expansion)
        self.expand_ratio = expand_ratio

        hidden_dim = int(round(inp * expand_ratio))*(2**expansion)
        layers = []
        if expand_ratio != 1:
            # pw
            self.first_conv = ConvBNReLU(inp, int(hidden_dim/(expansion+1)), kernel_size=1, groups=groups,
                                         use_relu = False)
            self.first_relu = nn.ReLU6(inplace=True)

        layers.extend([
            # dw
            ConvBNReLU(hidden_dim, hidden_dim, stride=stride, groups=hidden_dim),
            # pw-linear
            nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False, groups=groups*(2**expansion)),
            nn.BatchNorm2d(oup),
        ])
        self.conv = nn.Sequential(*layers)

    def forward(self, x):
        x_or = x
        if self.expand_ratio != 1:
            x = self.first_conv(x_or)
            x = self.first_relu(x)

        if(self.expansion):
            x_s = x.shape
            x = x.view(x_s[0],self.groups,int(x_s[1]/self.groups),x_s[2],x_s[3])
            x = torch.cat((x,x),dim=2).view(x_s[0],x_s[1]*2,x_s[2],x_s[3])

        if self.use_res_connect:
            out =  x_or + self.conv(x)
            return out
        else:
            out =  self.conv(x)
            return out


### MobileNet Tree Class
class MBNet_Tree(nn.Module):
    ### Initialization methods ###
    def __init__(self, width_multiplier, num_classes, expansions = [0,0,1,1,1,1,0]):
        super(MBNet_Tree, self).__init__()
        widen_factor = width_multiplier
        self.width_multiplier = width_multiplier
        self.widen_factor = widen_factor
        #
        block = InvertedResidual
        self.widen_factor = widen_factor
        self.num_classes = num_classes
        self.expansions = expansions
        round_nearest = 8
        input_channel = 16
        last_channel = 1280

        ###
        inverted_residual_setting = [
                # t, c, n, s, e
                [1, 16, 1, 1],
                [6, 24, 2, 2],
                [6, 32, 3, 2],
                [6, 64, 4, 2],
                [6, 96, 3, 1],
                [6, 160, 3, 2],
            ]

        self.expansions.append(0)
        # building first layer
        input_channel = _make_divisible(input_channel * widen_factor, round_nearest)
        self.last_channel = _make_divisible(last_channel * max(1.0, widen_factor), round_nearest)
        self.conv0 = ConvBNReLU(3, input_channel, stride=2)

        # building inverted residual blocks
        groups = 1
        exp = self.expansions
        layer_idx = 0
        self.g_num_models = []
        for t, c, n, s in inverted_residual_setting:
            output_channel = _make_divisible(c * widen_factor, round_nearest)
            layer = []
            self.g_num_models.append(groups)
            for i in range(n):
                stride = s if i == 0 else 1
                if(i==0):
                    layer.append(block(input_channel, output_channel, stride, expand_ratio=t, expansion=exp[layer_idx], groups=groups))
                    groups *= (2**exp[layer_idx])
                else:
                    layer.append(block(input_channel, output_channel, stride, expand_ratio=t, expansion=0, groups=groups))
                input_channel = output_channel
            layer = nn.Sequential(*layer)
            setattr(self,"layer" + str(layer_idx+1), layer)
            layer_idx += 1

        # initializing last layers
        self.last_layer = ConvBNReLU(input_channel*groups, input_channel*groups*6, kernel_size=1, groups=groups)
        self.fc_layer = ConvBNReLU(input_channel*groups*6, self.last_channel*groups, kernel_size=1, groups=groups)
        self.classifier= nn.Conv2d(self.last_channel*groups, num_classes*groups,1,groups=groups)
        self.num_models = groups


    ###
    def reset_weights(self,seed=0):
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out')
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            #elif isinstance(m, nn.Linear):
        nn.init.normal_(self.classifier.weight, 0, 0.01)
        nn.init.zeros_(self.classifier.bias)


    ##########################
    ### Forward methods ########
    ##########################
    def forward(self, x):
        n_samples = x.shape[0]
        out = x
        ###
        out = self.conv0(x)

        expansion_probs = []
        expansion_samples = []
        flop_computing = False
        num_expansions = 0
        for layer_idx in range(0,6):
            layer = getattr(self, "layer" + str(layer_idx+1))
            out = layer(out)

        # Last layers
        out = self.last_layer(out)
        out = F.avg_pool2d(out, 7)
        out = self.fc_layer(out)
        out = self.classifier(out)
        out = out.squeeze(2).squeeze(2)
        out_models = []


        ### Gather individual model outputs
        for m in range(0,self.num_models):
            out_model = out[:,self.num_classes*m:self.num_classes*(m+1)]
            out_models.append(out_model.unsqueeze(2))

        out_models = torch.cat(out_models, dim=2)
        return out_models

    #######################
    # Information methods #
    #######################
    def groups_stats(self, pred_individuals, ground_truth):
        # Compute prediction accuracy/std. dev. for ensembles with increasing size
        pred_individuals = pred_individuals.detach()
        total_groups = int(np.log2(self.num_models)+1)
        group_accuracies = torch.zeros(total_groups)
        group_std = torch.zeros(total_groups)
        for grouping in range(0, int(total_groups) ):
            group_size = 2**grouping
            pred_group = pred_individuals[:,:,0:group_size].mean(dim=2).squeeze()
            hard_labels = pred_group.argmax(dim=1)
            acc = (hard_labels==ground_truth).type_as(pred_group).mean()
            grouping_acc = acc
            group_accuracies[grouping] = grouping_acc*100
            group_std[grouping] = pred_individuals[:,:,0:group_size].std(dim=2).mean()
        group_std[0] = 0

        # Compute individual model accuracies
        models_acc =  torch.zeros(self.num_models)
        for idx_model in range(0,int(self.num_models) ):
            pred_model = pred_individuals[:,:,idx_model].squeeze()
            hard_labels = pred_model.argmax(dim=1)
            acc = (hard_labels==ground_truth).type_as(pred_model).mean()
            models_acc[idx_model] = acc.cpu().data.item()

        return group_accuracies.cpu().numpy(), group_std.cpu().numpy(), models_acc.cpu().numpy()


    ### Obtain FLOPs requirements for the model
    def get_tree_complexity(self, input_size):
        expansion_set = [[0,0,0,0,0,0,0],
                         [0,0,0,0,0,1,0],
                         [0,0,0,0,1,1,0],
                         [0,0,0,1,1,1,0],
                         [0,0,1,1,1,1,0]]

        # Compute FLOPs for each subtree with increasing complexity
        flops_groups = []
        for expansions in expansion_set:
                model = MBNet_Tree(self.width_multiplier,self.num_classes, expansions = expansions)
                flops = get_model_complexity_info(model, input_size, as_strings=False, print_per_layer_stat=False)
                flops_groups.append(flops)

        # FLOP Information string
        flops_str = "Computational Complexity of ResNet Tree - Total Models: " + str(self.num_models) + "\n"
        total_groups = int(np.log2(self.num_models)+1)
        for grouping in range(0, int(total_groups) ):
            group_size = 2**grouping
            flops_str += "Num Models: " + str(group_size) + " - FLOPs: " + str(int(flops_groups[grouping]/1e6)) + "M \n"

        return np.array(flops_groups), flops_str

    #####
    def extract_subnet(self, num_models):
        expansion_set = [[0,0,0,0,0,0,0],
                         [0,0,0,0,0,1,0],
                         [0,0,0,0,1,1,0],
                         [0,0,0,1,1,1,0],
                         [0,0,1,1,1,1,0]
        ]

        expansions = expansion_set[int(np.log2(num_models))]
        model = MBNet_Tree(self.width_multiplier, self.num_classes, expansions = expansions)
        for layer, layer_sub in zip(self.modules(),model.modules()):
            if isinstance(layer, nn.Conv2d):
                s = layer_sub.weight.shape
                b = layer.weight.data[0:s[0],:,:,:]
                b = b[:,0:s[1],:,:]
                layer_sub.weight.data = b
                #
                if(layer.bias is not None):
                    s = layer_sub.bias.shape
                    layer_sub.bias.data = layer.bias.data[0:s[0]]
            elif(isinstance(layer, nn.BatchNorm2d)):
                s = layer_sub.weight.shape
                layer_sub.weight.data = layer.weight.data[0:s[0]]
                layer_sub.bias.data = layer.bias.data[0:s[0]]
                layer_sub.running_mean.data = layer.running_mean.data[0:s[0]]
                layer_sub.running_var.data = layer.running_var.data[0:s[0]]
        return model

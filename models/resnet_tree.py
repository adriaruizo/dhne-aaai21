import torch
import torch.nn as nn
import torch.nn.init as init
import torch.nn.functional as F
from torch.autograd import Variable
import sys
import numpy as np
import random
import types
import math
from flops_counter import *

"""
 ResNet Tree Implementation

"""

### Basic Block Definition
class conv_layer(nn.Module):
    def __init__(self, in_planes, planes, stride, expand, num_models, use_shortcut):
        super(conv_layer, self).__init__()

        self.num_input_models = num_models
        self.expand = expand
        self.in_planes = in_planes
        self.planes = planes
        mult = 4

        ##
        out_c = int(planes/mult)
        self.conv1 = nn.Conv2d(in_planes*(expand+1), out_c, kernel_size=1, bias=False, groups=num_models*(expand+1))
        self.bn1 = nn.BatchNorm2d(out_c)
        ##
        self.conv2 = nn.Conv2d(out_c, out_c, kernel_size=3, bias=False, padding=1, stride=stride, groups=out_c)
        self.bn2 = nn.BatchNorm2d(out_c)
        ###
        self.conv3 = nn.Conv2d(out_c, planes, kernel_size=1, stride=1, bias=False, groups=num_models*(expand+1))
        self.bn3 = nn.BatchNorm2d(planes)

        self.shortcut = nn.Sequential()
        self.stride = stride
        self.relu = nn.ReLU(inplace=False)

        if(use_shortcut):
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes*(expand+1), int(planes), kernel_size=1, stride=stride, bias=False, groups=num_models*(expand+1)),
                nn.BatchNorm2d(planes),
            )

    def forward(self, x):
        x = self.relu(x)
        if(self.expand): # Branching block (increase number of groups by feature map concatenation)
            x_s = x.shape
            x = x.view(x_s[0],self.num_input_models,int(x_s[1]/self.num_input_models),x_s[2],x_s[3])
            x = torch.cat((x,x),dim=2).view(x_s[0],x_s[1]*2,x_s[2],x_s[3])
        else:
            x = self.relu(x)
        # Conv. block operations
        out = self.relu(self.bn1(self.conv1(x)))
        out = self.relu(self.bn2(self.conv2(out)))
        out =  self.bn3(self.conv3(out))
        out += self.shortcut(x)
        return out

### Resnet Tree Class ###
class ResNet_Tree(nn.Module):
    ### Initialization methods
    def __init__(self, width_multiplier, num_classes, expansions = (1,1,1,1)):
        super(ResNet_Tree, self).__init__()

        ###
        self.base_channels = int(16*width_multiplier)
        self.width_multiplier = width_multiplier
        self.bottleneck_multiplier = 4
        self.num_classes = num_classes

        # Block Definitions ( Total number = 4)
        nLayers = [3,4,4,5] # Number of layers per block
        strides = [1,2,2,1] # Stride used at the beggining of each block
        self.expansions = expansions # Determining whether the branching is applied at the begining of each block

        # Number of groups at each block depending on expansions definition
        self.g_num_models = [1]
        for idx in range(0,len(expansions)):
                self.g_num_models.append( self.g_num_models[idx]*2**(expansions[idx]))

        # Number of channels at the end of each block
        nStages = [int(self.base_channels)*1*self.g_num_models[0]*self.bottleneck_multiplier,
                   int(self.base_channels)*1*self.g_num_models[1]*self.bottleneck_multiplier,
                   int(self.base_channels)*2*self.g_num_models[2]*self.bottleneck_multiplier,
                   int(self.base_channels)*4*self.g_num_models[3]*self.bottleneck_multiplier,
                   int(self.base_channels)*4*self.g_num_models[4]*self.bottleneck_multiplier]


        # Initial convolutional block
        self.conv0 = nn.Sequential( nn.Conv2d(3, self.base_channels, kernel_size=3, padding=1, bias=False),
                                    nn.BatchNorm2d( self.base_channels),
                                    nn.ReLU(inplace=True),
                                    nn.Conv2d( self.base_channels, self.base_channels*self.bottleneck_multiplier,
                                               kernel_size=1, bias=False),
                                    nn.BatchNorm2d( self.base_channels*self.bottleneck_multiplier ),
        )

        for block_idx in range(0,4):
            block = self.rntree_block(nStages[block_idx], nLayers[block_idx], num_models = self.g_num_models[block_idx],
                                      stride = strides[block_idx], expansion=expansions[block_idx])
            setattr(self,"layer" + str(block_idx+1), block)

        # Final Linear CLassifier
        self.num_models = self.g_num_models[-1]
        self.linear = nn.Conv2d(nStages[-1], num_classes*self.num_models, kernel_size=1, bias=True, groups=self.num_models)


    ### Layer definition
    def rntree_block(self, in_planes, num_blocks, num_models, stride, expansion=1):
        strides = [stride] + [1]*(int(num_blocks-1))
        expands = [expansion] + [0]*(int(num_blocks-1))
        layers = []
        shortcuts = [1] + [0]*(int(num_blocks-1))
        for stride,expand,sc in zip(strides, expands,shortcuts):
            if(expand):
                out_planes = in_planes*2
            else:
                out_planes = in_planes
            out_planes = out_planes*(stride)
            layers.append(conv_layer(in_planes, out_planes, stride, expand, num_models,sc))
            num_models *= (expand+1)
            in_planes = out_planes

        return nn.Sequential(*layers)

    ### Weight Initialization
    def reset_weights(self,seed=0):
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        for layer in self.modules():
            if isinstance(layer, nn.Conv2d) or isinstance(layer, nn.Linear):
                nn.init.xavier_normal_(layer.weight)
                if(layer.bias is not None):
                    layer.bias.data.fill_(0)
            elif(isinstance(layer, nn.BatchNorm2d)):
                if(layer.bias is not None):
                    layer.bias.data.fill_(0)
                    layer.weight.data.fill_(1)

    ### Forward methods
    def forward(self, x):
        n_samples = x.shape[0]
        out = x
        ###
        out = self.conv0(x)

        expansion_probs = []
        expansion_samples = []
        for layer_idx in range(0,4):
            block = getattr(self, "layer" + str(layer_idx+1))
            out = block(out)
        ####
        out = F.avg_pool2d(out, 8)
        out = self.linear(out)
        out = out.squeeze(2).squeeze(2)
        out_models = []

        ### Gather individual model outputs
        for m in range(0,self.num_models):
            out_model = out[:,self.num_classes*m:self.num_classes*(m+1)]
            out_models.append(out_model.unsqueeze(2))
        out_models = torch.cat(out_models, dim=2)
        return out_models

    #######################
    # Information methods #
    #######################
    def groups_stats(self, pred_individuals, ground_truth):
        # Compute prediction accuracy/std. dev. for ensembles with increasing size
        pred_individuals = pred_individuals.detach()
        total_groups = int(np.log2(self.num_models)+1)
        group_accuracies = torch.zeros(total_groups)
        group_std = torch.zeros(total_groups)
        for grouping in range(0, int(total_groups) ):
            group_size = 2**grouping
            pred_group = pred_individuals[:,:,0:group_size].mean(dim=2).squeeze()
            hard_labels = pred_group.argmax(dim=1)
            acc = (hard_labels==ground_truth).type_as(pred_group).mean()
            grouping_acc = acc
            group_accuracies[grouping] = grouping_acc*100
            group_std[grouping] = pred_individuals[:,:,0:group_size].std(dim=2).mean()
        group_std[0] = 0


        # Compute individual model accuracies
        models_acc =  torch.zeros(self.num_models)
        for idx_model in range(0,int(self.num_models) ):
            pred_model = pred_individuals[:,:,idx_model].squeeze()
            hard_labels = pred_model.argmax(dim=1)
            acc = (hard_labels==ground_truth).type_as(pred_model).mean()
            models_acc[idx_model] = acc.cpu().data.item()*100

        return group_accuracies.cpu().numpy(), group_std.cpu().numpy(), models_acc.cpu().numpy()

    ### Obtain FLOPs requirements for the model
    def get_tree_complexity(self, input_size):
        expansion_set = [(0,0,0,0),
                         (0,0,0,1),
                         (0,0,1,1),
                         (0,1,1,1),
                         (1,1,1,1)]

        # Compute FLOPs for each subtree with increasing complexity
        flops_groups = []
        for expansions in expansion_set:
                model = ResNet_Tree(self.width_multiplier,  self.num_classes, expansions = expansions)
                flops = get_model_complexity_info(model, input_size, as_strings=False, print_per_layer_stat=False)
                flops_groups.append(flops)

        # FLOP Information string
        flops_str = "Computational Complexity of ResNet Tree - Total Models: " + str(self.num_models) + "\n"
        total_groups = int(np.log2(self.num_models)+1)
        for grouping in range(0, int(total_groups) ):
            group_size = 2**grouping
            flops_str += "Num Models: " + str(group_size) + " - FLOPs: " + str(int(flops_groups[grouping]/1e6)) + "M \n"


        return np.array(flops_groups), flops_str

    ###
    def extract_subnet(self, num_models):
        expansion_set = [(0,0,0,0),
                         (0,0,0,1),
                         (0,0,1,1),
                         (0,1,1,1),
                         (1,1,1,1)]

        expansions = expansion_set[int(np.log2(num_models))]
        model = ResNet_Tree(self.width_multiplier,  self.num_classes, expansions = expansions)
        model.cuda()
        for layer, layer_sub in zip(self.modules(),model.modules()):
            if isinstance(layer, nn.Conv2d):
                s = layer_sub.weight.shape
                b = layer.weight.data[0:s[0],:,:,:]
                b = b[:,0:s[1],:,:]
                layer_sub.weight.data = b
                #
                if(layer.bias is not None):
                    s = layer_sub.bias.shape
                    layer_sub.bias.data = layer.bias.data[0:s[0]]
            elif(isinstance(layer, nn.BatchNorm2d)):
                s = layer_sub.weight.shape
                layer_sub.weight.data = layer.weight.data[0:s[0]]
                layer_sub.bias.data = layer.bias.data[0:s[0]]
                layer_sub.running_mean.data = layer.running_mean.data[0:s[0]]
                layer_sub.running_var.data = layer.running_var.data[0:s[0]]

        return model

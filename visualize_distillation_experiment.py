import loader as loader
import argparse
from test_model import *
import numpy as np
from flops_counter import *
import matplotlib.pyplot as plt

if __name__=='__main__':
    # Evaluation on CIFAR datasets
    datasets = ['cifar10','cifar100']
    parser = argparse.ArgumentParser(description='Opts Parser')
    opts = loader.parse_opts(parser)
    wm = opts.width_multiplier
    # Define set of evaluated models
    tested_models = [# Base Model, Width Multiplier, Train type, Distillation weight,  Legend Name, Plot Lyne Style
                     ('RNTree', wm, 'IndL', 0.0, 'No Dist. (\u03B1=0)', 'r>-'),
                     ('RNTree', wm, 'CoD', 0.1, 'Dist. (\u03B1=0.1)','mp-'),
                     ('RNTree', wm, 'CoD', 0.25, 'Dist. (\u03B1=0.25)','c^-'),
                     ('RNTree', wm, 'CoD', 0.5, 'Dist. (\u03B1=0.5)','gs-'),
                     ('RNTree', wm, 'HCoD', 0.5, 'H. Dist. (\u03B1=0.5)','bo-'),
    ]


    subplot_dict2 = { 'cifar10' : 0, 'cifar100' : 1}
    # Evaluate all the models
    fig, axs = plt.subplots(1,4,figsize=(16,5))
    for dataset in datasets:
        opts.dataset = dataset
        dataset_dict = loader.init_dataset(opts)
        legends = []
        plots = []
        for model in tested_models:
            # Load models with the efined options
            opts.base_model = model[0]
            opts.width_multiplier = model[1]
            opts.train_loss = model[2]
            opts.alpha = model[3]
            legend_str = model[4]
            line_style = model[5]

            folders_dict,experiment_id = loader.init_model_folders(opts)
            model_dict = loader.init_model(opts, dataset_dict, folders_dict)
            model = model_dict['model']

            # Get Computational Complexity
            flops, _ = model.get_tree_complexity( (3,dataset_dict['input_size'][0],dataset_dict['input_size'][1]) )

            # Get results
            groups_acc, groups_std, models_acc = test(model, dataset_dict['test_loader'])

            # accuracy subplot
            axs[subplot_dict2[opts.dataset]*2].plot( flops, groups_acc,
                                                       line_style, label=legend_str)
            axs[subplot_dict2[opts.dataset]*2+1].plot( flops[1::], groups_std[1::],
                                                         line_style, label=legend_str)

    # Set plot visualization options
    axs[0].set_title(str('CIFAR10 (Acc.)') ,fontsize=20 )
    axs[2].set_title(str('CIFAR100 (Acc.)') ,fontsize=20 )
    axs[1].set_title(str('CIFAR10 (Std.)') ,fontsize=20 )
    axs[3].set_title(str('CIFAR100 (Std.)') ,fontsize=20 )

    for i in range(0,4):
        axs[i].tick_params(labelsize=14)
        axs[i].grid()
        axs[i].set_xlabel('#Flops (10e7)',fontsize=18)
        #axs[i].tight_layout()
    for i in range(0,4,2):
        axs[i].legend(fontsize=14)

    # Show/Save plot
    plt.tight_layout()
    plt.savefig('results/Distillation_Experiment_wm' + str(wm) + '.png',dpi=300)
    plt.show()

import pandas
import loader as loader
import argparse
from test_model import *
import numpy as np
import matplotlib.pyplot as plt
import pickle

if __name__=='__main__':
    # Load SOA results for the selected dataset
    parser = argparse.ArgumentParser(description='Opts Parser')
    opts = loader.parse_opts(parser)

    soa_results = pickle.load( open('results/soa_results.p','rb') )
    soa_results = soa_results[opts.dataset]

    # Model evaluation for the selected dataset
    if(opts.dataset=='cifar10' or opts.dataset=='cifar100'):
        tested_models = [('RNTree', 1.0, 'HCoD', 'HNE (Ours)'),
                         ('RNTree', 0.5, 'HCoD', 'HNE Small (Ours)')]
    elif(opts.dataset=='imagenet'):
        tested_models = [('MBNTree', 1.4, 'HCoD', 'HNE (Ours)')]

    if(opts.dataset=='cifar10'):
        order_plot = ['Graph HyperNet','MSDNet','CNMM Small','CNMM','RANet','HNE Small (Ours)','HNE (Ours)' ]
        line_styles = ['-P','->','-gs','-mv','-k*','-.bD','-bo']
        min_acc = 89
    elif(opts.dataset=='cifar100'):
        order_plot = ['Adaptive Deep Net.','MSDNet','CNMM Small','CNMM','RANet','HNE Small (Ours)','HNE (Ours)' ]
        line_styles = ['-P','->','-gs','-mv','-k*','-.bD','-bo']
        min_acc = 65
    elif(opts.dataset=='imagenet'):
        line_styles = ['-mv','-k*','-bo']
        order_plot = ['MSDNet','RANet','HNE (Ours)']
        min_acc = 0

    # Evaluate all the models
    fig, axs = plt.subplots(1,1,figsize=(9,10))
    dataset_dict = loader.init_dataset(opts)
    legends = []
    plots = []
    for model in tested_models:
        # Load models with the efined options
        opts.base_model = model[0]
        opts.width_multiplier = model[1]
        opts.train_loss = model[2]
        legend_name = model[3]

        folders_dict,experiment_id = loader.init_model_folders(opts)
        model_dict = loader.init_model(opts, dataset_dict, folders_dict)
        model = model_dict['model']

        # Get Computational Complexity
        flops, _ = model.get_tree_complexity( (3,dataset_dict['input_size'][0],dataset_dict['input_size'][1]) )
        flops = flops/1e6

        # Get results
        groups_acc, groups_std, models_acc = test(model, dataset_dict['test_loader'])
        if(opts.dataset!='imagenet'):
            flops = flops[0:4]
            groups_acc = groups_acc[0:4]
            groups_std = groups_std[0:4]
        #
        soa_results[legend_name] = np.concatenate( (flops[np.newaxis],
                                                    groups_acc[np.newaxis]), axis=0)


    # Plot results
    idx_style = 0
    for method in order_plot:
        results = soa_results[method]
        results = results[:,np.where(results[1,:]>min_acc)[0]]
        axs.plot(results[0,:]/100, results[1,:], line_styles[idx_style], label=method, markersize=7)
        idx_style += 1

    # Set plot visualization options
    axs.set_title(opts.dataset.upper() + ' (Acc.)' ,fontsize=34 )
    axs.set_xlabel('#Flops (10e7)',fontsize=30)

    axs.tick_params(labelsize=30)
    axs.legend(prop={'size': 28 }, loc =4)
    axs.grid()

    # Show/Save plot
    plt.tight_layout()
    plt.savefig('results/SOA_Comparison_' + opts.dataset.upper() + '.png',dpi=600)
    plt.show()

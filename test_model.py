import torch
import torch.nn
from utils import *
import torch.optim as optim
import time

### Compute dataset predictions and compute results
def test(model,data_loader):
    print('----------------------------------------')
    print("Processing dataset... ")
    ### Init restuls variables
    acc_ens = AverageMeter('Ens. Acc@1', ':6.2f')
    batch_time = AverageMeter('Ims/s', ':4.0f')
    progress = ProgressMeter(len(data_loader), batch_time, acc_ens, prefix="Epoch: [{}]".format(0) + '(Test)')
    model.eval()

    dataset_predictions = []
    dataset_targets = []

    # Process batch
    end = time.time()
    for batch_idx, (inputs, targets) in enumerate(data_loader):
        inputs = inputs.cuda()
        targets = targets.cuda()
        ### Model forward
        with torch.no_grad():
            outputs_models = model(inputs)

            # Compute full ensemble accuracy
            ensemble_mean = outputs_models.mean(dim=2).squeeze().detach()
            # Update Average Meter
            acc1, acc5 = accuracy(ensemble_mean, targets, topk=(1, 5))
            acc_ens.update(acc1.data.item(), inputs.size(0))

        dataset_predictions.append(outputs_models)
        dataset_targets.append(targets)
        batch_time.update(inputs.shape[0] / ( time.time() - end))
        end = time.time()
        if(batch_idx%1==0):
            progress.print(batch_idx)
    progress.print(batch_idx)
    # Compute model results
    dataset_predictions = torch.cat(dataset_predictions, dim=0)
    dataset_targets = torch.cat(dataset_targets, dim=0)
    group_acc, group_std, models_acc = model.groups_stats(dataset_predictions, dataset_targets)

    return group_acc, group_std, models_acc

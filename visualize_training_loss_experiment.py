import loader as loader
import argparse
from test_model import *
import numpy as np
from flops_counter import *
import matplotlib.pyplot as plt

if __name__=='__main__':
    # Evaluation on CIFAR datasets
    datasets = ['cifar10','cifar100']
    parser = argparse.ArgumentParser(description='Opts Parser')
    opts = loader.parse_opts(parser)

    # Define set of evaluated models
    tested_models = [# Base Model, Width Multiplier, Train type,  Legend Name, Plot Lyne Style
                     ('RNTree', 1.0, 'IndL', 'Ind. Loss', 'r>-'), # Independent loss for each network
                     ('RNTree', 1.0, 'HL', 'H. Loss','kP-'), # Hierarchical loss
                     ('RNTree', 1.0, 'HCoD', 'H. Distillation','bo-'), # Independent Loss + Hierarchical Co-distillation

                     # DHNE Small
                     ('RNTree', 0.5, 'IndL', 'Ind. Loss', 'r>-'), # Independent loss for each network
                     ('RNTree', 0.5, 'HL', 'H. Loss','kP-'), # Hierarchical loss
                     ('RNTree', 0.5, 'HCoD', 'H. Distillation','bo-'), # Independent Loss + Hierarchical Co-distillation
    ]

    subplot_dict1 = { 1.0 : 0, 0.5 : 1}
    subplot_dict2 = { 'cifar10' : 0, 'cifar100' : 1}
    # Evaluate all the models
    fig, axs = plt.subplots(1,4,figsize=(18,4))
    for dataset in datasets:
        opts.dataset = dataset
        dataset_dict = loader.init_dataset(opts)
        legends = []
        plots = []
        for model in tested_models:
            # Load models with the efined options
            opts.base_model = model[0]
            opts.width_multiplier = model[1]
            opts.train_loss = model[2]
            legend_str = model[3]
            line_style = model[4]

            folders_dict,experiment_id = loader.init_model_folders(opts)
            model_dict = loader.init_model(opts, dataset_dict, folders_dict)
            model = model_dict['model']

            # Get Computational Complexity
            flops, _ = model.get_tree_complexity( (3,dataset_dict['input_size'][0],dataset_dict['input_size'][1]) )

            # Get results
            groups_acc, groups_std, models_acc = test(model, dataset_dict['test_loader'])

            # accuracy subplot
            axs[2*subplot_dict2[opts.dataset]+subplot_dict1[opts.width_multiplier]].plot( flops, groups_acc,
                                                                                          line_style, label=legend_str)

    # Set plot visualization options
    axs[0].set_title(str('CIFAR10') ,fontsize=18 )
    axs[1].set_title(str('CIFAR10 (Small)') ,fontsize=18 )
    axs[2].set_title(str('CIFAR100') ,fontsize=18 )
    axs[3].set_title(str('CIFAR100 (Small)') ,fontsize=18 )

    axs[0].set_xlabel('#Flops (10e7)',fontsize=14)
    axs[1].set_xlabel('#Flops (10e7)',fontsize=14)
    axs[2].set_xlabel('#Flops (10e7)',fontsize=14)
    axs[3].set_xlabel('#Flops (10e7)',fontsize=14)
    #
    axs[0].set_ylabel('Accuracy',fontsize=18)

    for i in range(0,4):
        axs[i].tick_params(labelsize=12)
        axs[i].legend(fontsize=14)
        axs[i].grid()

    # Show/Save plot
    plt.savefig('results/Train_Loss_Experiment.png',dpi=300)
    plt.show()

import argparse
import os

### Option parser
def parse_opts(parser, args_str=None):
    #### Dataset
    parser.add_argument('-d','--dataset', type=str, default='cifar10',
                        help='Dataset (default: cifar10)')
    parser.add_argument('-dp','--dataset_path', type=str, default='Data/',
                        help='Dataset (default: cifar10)')
    #### Model Definition
    parser.add_argument('-m','--base_model', type=str, default='RNTree',
                        help='Base model (CIFAR-> RNTree , ImageNet->MBNTree - default: RNTree')
    parser.add_argument('-w','--width_multiplier', type=float, default=1.0,
                        help='Channel Number Multiplier (default: 1.0 - Small RNTree: 0.5)')

    ### Training options
    parser.add_argument('-tl','--train_loss', type=str, default='HCoD',
                        help='Trainining loss (options: IndL, HLoss, Ind_HLoss, CoD, HCoD - default: HCoD)')
    parser.add_argument('-a','--alpha', type=float, default=0.5,
                        help='Distillation Weight Alpha (default: 0.5)')
    parser.add_argument('-b','--batch_size', type=int, default=0,
                        help='Batch Size (default: dataset dependent)')

    #### Other
    parser.add_argument('-ef','--subfolder', type=str, default='default',
                        help='Saved models subfolder (default: default)')
    parser.add_argument('-s','--seed', type=int, default=0,
                        help='Random seed (default: 0)')

    if args_str is None:
        args = parser.parse_args()
    else:
        args = parser.parse_args(args_str)
    return args

### Generate experiment id and folders
def init_model_folders(opts):
    ###
    dataset_id = opts.dataset
    if(opts.batch_size > 0):
        dataset_id =  dataset_id + '_BS' + str(opts.batch_size)
    ###
    model_id = opts.base_model + 'x' + str(opts.width_multiplier)
    model_id += "_" + opts.train_loss

    if(opts.train_loss == 'CoD'):
        model_id += "_A" + str(opts.alpha)

    experiment_id =  dataset_id + '_' + model_id

    folders_dict = {}
    folders_dict['model_id'] = experiment_id
    folders_dict['model'] = "saved_models/" + '/' + opts.subfolder + '/' + opts.dataset +  "/" + experiment_id

    if not os.path.exists(folders_dict['model']):
        os.makedirs(folders_dict['model'])

    ###

    if not os.path.exists(folders_dict['model']):
        os.makedirs(folders_dict['model'])

    return folders_dict, experiment_id

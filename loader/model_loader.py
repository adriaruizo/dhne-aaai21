import torch
import torch.nn as nn
import torch.optim as optim
import models as m
import glob
import sys
import torch.nn.functional as F
import pickle


###################################################################
def init_model(opts, dataset_dict, folders_dict, lr=0.1):
    print('----------------------------------------')
    print('Initializing Model...')
    print('Model Id.: ' + folders_dict['model_id'])
    print('Model Path: ' + folders_dict['model'])
    print('Batch Size: ' + str(dataset_dict['batch_size']))
    ### Initialize different base model architectures
    if(opts.base_model=='RNTree'):
        model = m.ResNet_Tree(opts.width_multiplier, dataset_dict['num_classes'])
    if(opts.base_model=='MBNTree'):
        model = m.MBNet_Tree(opts.width_multiplier, dataset_dict['num_classes'])

    model.cuda()
    ### Weight initialization
    model.reset_weights(opts.seed)

    ### Model optimizer Initialization
    if(dataset_dict['name'] == 'cifar10' or dataset_dict['name'] == 'cifar100'):
        lr = 0.1
        optimizer_model = optim.SGD(model.parameters(), weight_decay=5e-4,
                                    lr = lr, nesterov=True, momentum = 0.9)
    else:
        lr = 0.1
        optimizer_model = optim.SGD(model.parameters(), weight_decay=1e-5,
                                    lr = lr, nesterov=True, momentum = 0.9)

    ##### LOAD SAVED MODEL AND TRAINING INFO #####
    model_folder = folders_dict['model']
    epoch = glob.glob(model_folder + '/*last.t7').__len__() - 1
    train_scores = []
    test_scores = []

    ### Load model from last epochs if possible
    if(epoch > -1):
        chk = torch.load(model_folder + '/model_last.t7')
        epoch = chk['epoch']
        print("Loading model from epoch " + str(epoch))
        train_scores = chk ['train_scores']
        test_scores = chk['test_scores']
        model.load_state_dict(chk['model'] , strict=False)
        try:
            optimizer_model.load_state_dict(chk['optimizer_model'] )
        except:
            print('Problem loading optimizer - Default Initialization used')
    epoch = epoch+1

    ### Store model info into dictionary
    model_dict = {}
    model_dict['epoch'] = epoch
    model_dict['model'] = model
    model_dict['optimizer_model'] = optimizer_model
    model_dict['train_scores'] = train_scores
    model_dict['test_scores'] = test_scores

    return model_dict

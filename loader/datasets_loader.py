import torch
import torchvision
import torchvision.transforms as transforms
import torch.utils.data as data
import numpy as np
import math
import random
from PIL import Image
import types
import glob


###################################################################
def init_dataset(opts):
    ### Dataset loaders
    print('----------------------------------------')
    print('Loading Dataset: ' + opts.dataset)
    data_path = 'Data/' + opts.dataset

    ### CIFAR100
    if(opts.dataset=='cifar100'):
        # Dataset Information
        batch_size = 128
        if(opts.batch_size>0):
            batch_size = opts.batch_size
        num_classes = 100
        epochs = 200
        input_size = [32,32]

        # Data augmentation
        transform_train = transforms.Compose([
            transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)),])
        transform_test = transforms.Compose([transforms.ToTensor(),
                                             transforms.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)),])

        # DataLoaders
        train_set = torchvision.datasets.CIFAR100(root=opts.dataset_path + "/CIFAR100", train=True,
                                                  download=True, transform=transform_train)
        test_set = torchvision.datasets.CIFAR100(root=opts.dataset_path + "/CIFAR100", train=False,
                                                 download=True, transform=transform_test)
        test_loader = data.DataLoader(test_set, num_workers=8,
                                      shuffle=False, batch_size=batch_size)
        train_loader = data.DataLoader(train_set, num_workers=8,
                                       shuffle=True, batch_size=batch_size)

    ### CIFAR10
    elif(opts.dataset=='cifar10'):
        # Dataset Information
        batch_size = 128
        if(opts.batch_size>0):
            batch_size = opts.batch_size
        num_classes = 10
        epochs = 200
        input_size = [32,32]

        # Data augmentation
        transform_train = transforms.Compose([
            transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize((0.4914, 0.4824, 0.4467), (0.2471, 0.2435, 0.2616)),
            ])

        transform_test = transforms.Compose([transforms.ToTensor(),
                                             transforms.Normalize((0.4914, 0.4824, 0.4467), (0.2471, 0.2435, 0.2616)),])

        # Dataloaders
        train_set = torchvision.datasets.CIFAR10(root=opts.dataset_path + "/CIFAR10", train=True,
                                                 download=True, transform=transform_train)
        test_set = torchvision.datasets.CIFAR10(root=opts.dataset_path + "/CIFAR10", train=False,
                                               download=True, transform=transform_test)

        test_loader = data.DataLoader(test_set, num_workers=8,
                                      shuffle=False, batch_size=batch_size)
        train_loader = data.DataLoader(train_set, num_workers=8,
                                       shuffle=True, batch_size=batch_size)
    # Imagenet
    elif(opts.dataset=='imagenet'):
        ###
        batch_size = 256
        if(opts.batch_size>0):
            batch_size = opts.batch_size

        input_size = [224,224]
        transform_test = transforms.Compose([transforms.Resize(256),
                                             transforms.CenterCrop(224),
                                             transforms.ToTensor(),
                                             transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),])

        transform_train = transforms.Compose([transforms.RandomResizedCrop(224, scale=(0.25 ,1.0)),
                                              transforms.RandomHorizontalFlip(),
                                              transforms.ToTensor(),
                                              transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),])

        test_set = torchvision.datasets.ImageFolder(root=opts.dataset_path + "/val", transform=transform_test)
        train_set = torchvision.datasets.ImageFolder(root=opts.dataset_path + "/train", transform=transform_train)

        num_classes = 1000
        ### DataLoaders
        test_loader = data.DataLoader(test_set, num_workers=0, shuffle=False, batch_size=128)
        train_loader = data.DataLoader(train_set, num_workers=32, shuffle=True, batch_size=batch_size)

        epochs = 120

    ### Return loaded dataset
    dataset_loader_dict = {}
    dataset_loader_dict['name'] = opts.dataset
    dataset_loader_dict['input_size'] = input_size
    dataset_loader_dict['batch_size'] = batch_size
    dataset_loader_dict['train_set'] = train_set
    dataset_loader_dict['test_set'] = test_set
    dataset_loader_dict['test_loader'] = test_loader
    dataset_loader_dict['train_loader'] = train_loader
    dataset_loader_dict['num_classes'] = num_classes
    dataset_loader_dict['num_epochs'] = epochs

    ###
    return dataset_loader_dict

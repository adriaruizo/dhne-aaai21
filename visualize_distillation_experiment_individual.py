import loader as loader
import argparse
from test_model import *
import numpy as np
from flops_counter import *
import matplotlib.pyplot as plt

if __name__=='__main__':
    # Evaluation on CIFAR datasets
    parser = argparse.ArgumentParser(description='Opts Parser')
    opts = loader.parse_opts(parser)
    dataset = opts.dataset
    wm = opts.width_multiplier
    # Define set of evaluated models
    tested_models = [# Base Model, Width Multiplier, Train type, Distillation weight,  Legend Name, Plot Lyne Style
                     ('RNTree', wm, 'IndL', 0.0, 'No Dist. (\u03B1=0)', 'r>-'),
                     ('RNTree', wm, 'CoD', 0.25, 'Dist. (\u03B1=0.25)','gs-'),
                     ('RNTree', wm, 'CoD', 0.5, 'Dist. (\u03B1=0.5)','c^-'),
                     ('RNTree', wm, 'HCoD', 0.5, 'H. Dist. (Ours)','bo-'),
    ]


    # Evaluate all the models
    min_acc = 100
    max_acc = 0
    fig, axs = plt.subplots(1,4,figsize=(16,5))
    for i in range(0,1):
        opts.dataset = dataset
        dataset_dict = loader.init_dataset(opts)
        legends = []
        plots = []
        for idx,model in enumerate(tested_models):
            # Load models with the efined options
            opts.base_model = model[0]
            opts.width_multiplier = model[1]
            opts.train_loss = model[2]
            opts.alpha = model[3]
            legend_str = model[4]
            line_style = model[5]

            folders_dict,experiment_id = loader.init_model_folders(opts)
            model_dict = loader.init_model(opts, dataset_dict, folders_dict)
            model = model_dict['model']

            # Get Computational Complexity
            flops, _ = model.get_tree_complexity( (3,dataset_dict['input_size'][0],dataset_dict['input_size'][1]) )

            # Get results
            groups_acc, _, models_acc = test(model, dataset_dict['test_loader'])
            min_acc = min(min_acc,groups_acc.min().item())
            max_acc = max(max_acc,groups_acc.max().item())

            # accuracy subplot
            axs[idx].plot( np.asarray([1,2,4,8,16]), np.asarray(groups_acc), line_style)
            axs[idx].bar(np.arange(1,17), models_acc )
            axs[idx].set_xlim(0.5,16.5)
            axs[idx].set_title(legend_str ,fontsize=20 )

    # Set plot visualization options
    for i in range(0,4):
        axs[i].tick_params(labelsize=14)
        axs[i].grid()
        axs[i].set_xlabel('Ens. Size',fontsize=18)
        if(dataset=='cifar10'):
            axs[i].set_ylim(min_acc-0.25,max_acc+0.25)
        if(dataset=='cifar100'):
            axs[i].set_ylim(min_acc-1.25,max_acc+1.25)
    #for i in range(0,4,2):
        #axs[i].legend(fontsize=14)

    # Show/Save plot
    plt.tight_layout()
    plt.savefig('results/Dist_Experiment_models_' + str(wm) + '_' + dataset + '.png',dpi=300)
    plt.show()

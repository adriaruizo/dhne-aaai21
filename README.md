# Distilled Hierarchical Neural Ensembles

This repository contains PyTorch implementation of the Distilled Hierarchical Neural Ensembles framework presented in the paper ["*Anytime Inference with Distilled Hierarchical Neural Ensembles*"](arxiv-url) accepted in the AAAI Conference on Artificial Intelligence (AAAI-21). The provided code allows to reproduce the main results of the paper. Training code for HNE will be also made available soon.


## Contents

1. [Abstract](#abstract)
2. [Setup and Dependencies](#Setup and Dependencies)
3. [Usage and Results](#Usage and Results)

## Abstract

Inference in deep neural networks can be  computationally expensive, and networks capable of anytime inference are  important in  scenarios where the amount of compute or input data varies over time. In such networks the inference process can interrupted to provide a result faster, or continued to obtain a more accurate result. We propose Hierarchical Neural Ensembles (HNE), a novel framework to embed an ensemble of multiple networks in a hierarchical tree structure, sharing intermediate layers. In HNE we control the complexity of inference on-the-fly by evaluating more or less models in the ensemble.Our second contribution is a novel hierarchical distillation method to boost the predictions  of small ensembles. This approach leverages the nested structure of our ensembles, to optimally allocate accuracy and diversity across the individual models. Our experiments show that, compared to previous anytime inference models, 
HNE provides state-of-the-art accuracy-computation trade-offs on the  CIFAR-10/100 and ImageNet datasets. 

<img src="Intro.png" width="40%">

Figure 1: (Top) (Top) HNE shares parameters and computation in a hierarchical % tree-structured 
manner.Tree leafs represent  separate models in the ensemble. 
Anytime inference is obtained via  depth-first traversal of the tree, and using at any given time the ensemble prediction of the $N$  models evaluated so far. (Bottom) Hierarchical distillation leverages the full ensemble to supervise parts of the tree that are used in small ensembles.

## Setup and Dependencies

- [Python3](https://www.python.org/downloads/)
```
conda create -n dhne python=3.7 anaconda
conda activate dhne
```
- [PyTorch(1.1.0)](http://pytorch.org)
```
conda install pytorch==1.1.0 torchvision==0.3.0 cudatoolkit=10.0 -c pytorch
conda install "pillow<7"
```

For ImageNet experiments download [ImageNet](https://www.image-net.org/challenges/LSVRC/2012/)
and run the [script](https://gist.github.com/BIGBALLON/8a71d225eff18d88e469e6ea9b39cef4) to prepare train and validation folders.

## Reproducing Paper Results
We provide different scripts to reproduce the results reported in the paper.  Models are evaluated and plots are shown and saved in the folder "results". Trained weights for all the evaluated models are saved in the folder saved_models/default.

### Evaluation of different learning strategies for DHNE
We compare the results obtained by using different optimization losses to train the Hierarchical Neural Ensembles:
(1) An independent loss for each model (Ind. Loss). 
(2) A hierarchical loss explicitly maximizing the accuracy of nested ensembles (H. Loss)
(43) The proposed hierarchical distillation approach (H. Distillation).
We evaluate the HNE based on ResNet and the "smaller" version of it.

Results reported in the paper can be obtained and visualized exectuing:
```
python visualize_training_loss_experiment.py
```
Plotted results:
<img src="results/Train_Loss_Experiment.png" align="center">

### Comparing Hierarchical and Standard Co-Distillation
We compare the results obtained with the proposed of the Hierarchical distillation  scheme (H. Dist.) and the obtained with standard distillation ( Dist. ).
For standard distillation we use different weights for the distillation term. We evaluate the HNE based on ResNet.

Rerported results can be obtained and visualized exectuing:
```
python visualize_distillation_experiment.py -w 0.5
```
Plotted results:
<img src="results/Distillation_Experiment_wm0.5.png" align="center">

CIFAR100 results regardin the accuracy of the individual models in the ensemble can be reproduced by executing:
```
python visualize_distillation_experiment.py -w 0.5 -d cifar100
```
Plotted results:
<img src="results/Dist_Experiment_models_0.5_cifar100.png" align="center">

### Comparison with state-of-the-art models

We compare the proposed DHNE to previous state-of-the-art methods in CIFAR and ImageNet datasets. 
DHNE models are evaluated with the following scripts and the results of compared methods are loaded from "results/soa_results.p"
#### CIFAR10

Execute for CIFAR10:
```
python visualize_soa_comparison.py -d cifar10
```
<img src="results/SOA_Comparison_CIFAR10.png" width="40%" align="center">

#### CIFAR100

Execute for CIFAR100:
```
python visualize_soa_comparison.py -d cifar100
```
<img src="results/SOA_Comparison_CIFAR100.png" width="40%" align="center">

#### ImageNet
Execute for Imagenet (TO UPDATE):
```
python visualize_soa_comparison.py -d imagenet -dp IMAGENET_FOLDER
```
<img src="results/SOA_Comparison_IMAGENET.png" width="40%" align="center">


